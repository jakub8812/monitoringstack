# description -> TO-DO

## Prometheus Application Stack (vm-prom-stack)
### To run the application stack, just enter the following command (make sure you are in the prometheus application stack folder)
> docker-compose up -d

### Then you can log in to http://<< IP-Address >>:9090 and in the "Status" tab enter "Targets". If cadvisor, node_exporter, prometheus will have "State" == "UP" then everything works fine and you can go to grafana at: http://<< IP-Address >>:9090:3000, default login and password is: admin (password is the same)

import cadvisor full: 14282
import node exporter full: 1860

## ELK Stack -> TO-DO
